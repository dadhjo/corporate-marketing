ADOBE INDESIGN PRINTING INSTRUCTIONS FOR SERVICE PROVIDER REPORT

PUBLICATION NAME: gitlab-federal-capabilities-statement.indd

PACKAGE DATE: 10/15/19 4:48 PM
Creation Date: 10/15/19
Modification Date: 10/15/19

CONTACT INFORMATION

Company Name: 
Contact: 
Address: 





Phone: 
Fax: 
Email: 

SPECIAL INSTRUCTIONS AND OTHER NOTES






External Plug-ins 0
Non Opaque Objects :On Page1

FONTS
7 Fonts Used; 0 Missing, 0 Embedded, 0 Incomplete, 1 Protected

Fonts Packaged
- Name: Lato-Black; Type: OpenType TrueType, Status: OK
- Name: Lato-Bold; Type: OpenType TrueType, Status: OK
- Name: SourceSansPro-Bold; Type: OpenType TrueType, Status: OK
- Name: SourceSansPro-Regular; Type: OpenType TrueType, Status: OK
- Name: SourceSansPro-Regular; Type: OpenType TrueType, Status: OK
- Name: SourceSansPro-SemiBold; Type: OpenType TrueType, Status: OK
Fonts Not Packaged
- Name: SourceSansPro-Semibold; Type: OpenType Type 1, Status: Synced


COLORS AND INKS
4 Process Inks; 0 Spot Inks

- Name and Type: Process Cyan; Angle: 0.000; Lines/Inch: 0.000
- Name and Type: Process Magenta; Angle: 0.000; Lines/Inch: 0.000
- Name and Type: Process Yellow; Angle: 0.000; Lines/Inch: 0.000
- Name and Type: Process Black; Angle: 0.000; Lines/Inch: 0.000


LINKS AND IMAGES
(Missing & Embedded Links Only)
Links and Images: 3 Links Found; 0 Modified, 0 Missing 0 Inaccessible
Images: 0 Embedded, 1 use RGB color space


PRINT SETTINGS
PPD: N/A, (HP ENVY 4500 series)
Printing To: Printer
Number of Copies: 1
Reader Spreads: No
Even/Odd Pages: Both
Pages: All
Proof: No
Tiling: None
Scale: 100%, 100%
Page Position: Upper Left
Print Layers: Visible & Printable Layers
Printer's Marks: None
Bleed: 0 in, 0 in, 0 in, 0 in
Color: Composite RGB
Trapping Mode: None
Send Image Data: Optimized Subsampling
OPI/DCS Image Replacement: No
Page Size: Letter
Paper Dimensions: 8.5 in x 11 in
Orientation: Landscape
Negative: No
Flip Mode: Off


FILE PACKAGE LIST

1. gitlab-federal-capabilities-statement.indd; type: Adobe InDesign publication; size: 2196K
2. gitlab-federal-capabilities-statement.idml; type: InDesign Markup Document; size: 151K
3. gitlab-federal-capabilities-statement.pdf; type: Adobe Acrobat Document; size: 2854K
4. Lato-Black.ttf; type: Font file; size: 111K
5. Lato-Bold.ttf; type: Font file; size: 118K
6. SourceSansPro-Bold.ttf; type: Font file; size: 284K
7. SourceSansPro-Regular.ttf; type: Font file; size: 286K
8. SourceSansPro-SemiBold.ttf; type: Font file; size: 285K
