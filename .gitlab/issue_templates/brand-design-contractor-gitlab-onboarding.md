## Welcome to GitLab's Brand Design team!

**GitLab onboarding:**

- [ ] Your Onboarding Buddy is `[@onboarding-buddy]`
- [ ] Review [Your issue board](#) - reviewed in intro call
- [ ] @ mention yourself in this issue.
  - [ ] Confirm you received an email notification. Click through the email. Confirm you were taken to the issue.
  - [ ] Confirm you received a To-Do notification in your To-Do list in the GitLab app (checkmark icon in top-right of navigation bar). click through the notification to confirm you were taken to the issue.

**Issue orientation:**
- [ ] Note that you’re assigned the issue in the upper right.
- [ ] Note the due date/milestone
- [ ] Post a comment to `[@manager]` and Onboarding Buddy with a question or comment; we'll answer async.
- [ ] When you receive our response, reply with a thumbs-up or emoji of your choice. We always acknowledge input from others and set expectations as to when we can get back to them.
- [ ] Note the issue description - this should include all the information you need to complete the request or your specific task.
  * Keep this up to date with the scope of your project, links, etc. The description should be the single source of truth about a project.
- [ ] Note the `Designs` section just below the issue description.
  - [ ] Upload an image and post a point-of-interest (POI) comment and @ mention your Onboarindg Buddy.
  - [ ] `[@onboarding-buddy]` to respond.

**Slack:**
- [ ] Say hello on slack - #marketing-design
  * Use this to ask questions, provide a quick update, or share work for review/feedback with the team.

**Other resources:**
  - [ ] Review [Brand Design handbook](https://about.gitlab.com/handbook/marketing/corporate-marketing/brand-activation/brand-design/)
  - [ ] Review [Brand Resources and Standards](https://about.gitlab.com/handbook/marketing/corporate-marketing/brand-activation/brand-standards/)
  - [ ] Review [GitLab's values](https://about.gitlab.com/handbook/values/)

/label ~"mktg-status::wip" ~"design" ~"Corporate-Marketing"
