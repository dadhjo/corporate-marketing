## Campaign goal(s):

<!-- list out goals here -->


### Asset targets:

<!-- e.g. landing page, webcast, ebook, etc. Please include landing page urls or issues/MRs if not created yet -->


### Design applications:

<!-- list out anticipated creative assets (display ads, social, email/web graphics) -->

### Audience:



### Messaging framework:

<!-- this can be WIP, but gives us an idea what copy might be surrounding visuals -->


/label ~"design" ~"mktg-status::triage" ~"Corporate Marketing"

/assign @luke @amittner
