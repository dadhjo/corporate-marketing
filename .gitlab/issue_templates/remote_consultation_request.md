<!-- Use this template to request a consultation from the All-Remote team on topics related to remote work. -->

Please title this issue **Remote Consultation: Name of Company**


## :goal: OKR or Sales goal supported

<!-- Please specify any OKRs or Sales objectives that this can support. -->


## :bulb: Background

* Name of organization: `Add organization here`
* Contact name and information: `Add contact info here`
* SFDC: `If you have it, link to the Salesforce info on this prospect/client`

<!-- Please add a few sentences about how this idea came up. Include any connections we have, e.g. did someone from another company reach out to someone at GitLab to ask about remote work? -->


## :pencil: Details

All-Remote team to fill out this section:

**Goals:**
Choose as many as relevant.
* [ ] Visibility
* [ ] GitLab thought leadership
* [ ] Backlinking (requesting the organization mentions and links back to GitLab's remote resources)
* [ ] Sales support: Enterprise
* [ ] Sales support: SMB
* [ ] Content creation

**Commitment:**
* [ ] Private consultation call with leadership
* [ ] Private Q&A/presentation on remote work
* [ ] Public/shareable video or webcast
* [ ] Custom PathFactory track
* [ ] Other


## :calendar: Date and time
If a synchronous event is scheduled, add details here.

* Date/time: 
* Duration and details:
* Link to presentation deck:


## :mega: Spread the Word!

* **Social:** `Link to social issue`


## :school: PathFactory track

`Link to PathFactory track`








/assign @jessicareeder @dmurph @bbula
/confidential
/label ~"All-Remote Team"
