### Goal: Determine design requirements for necessary assets for `[Campaign Name]` campaign

Coordinating with all team members who would require design assets to be created of @gl-design, and compile a final list of assets to be created.

## DRIs
<!-- Add DRIs for each team / add/remove teams based on your campaign plan. -->

- Marketing Programs (MPM) - `[@DRI]`

- Content - `[@DRI]`

- Digital Marketing Programs (DMP) - `[@DRI]`

- Public Relations (PR) - `[@DRI]`

- Awareness Campaign Manager (ACM) - `[@DRI]`

- Social - `[@DRI]`

- Digital / Web - `[@DRI]`


## Design requirements by team
<!-- Create a copy of the Google Sheets template below and update the url -->

[Design Requirements Template - Make a copy & update URL](https://docs.google.com/spreadsheets/d/1dd_tZkz4cnAPR2xb65GBzxxOtrx9bmlnDKh6h6hL1m0/edit?usp=sharing)

/label ~"design" ~"mktg-status::triage" ~"Corporate Marketing" 

/assign @luke
