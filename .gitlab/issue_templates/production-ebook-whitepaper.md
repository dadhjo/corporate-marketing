This issue is for the design team to design an ebook or whitepaper NOT the content team to create one

## Overview
INSERT NOTES FOR DESIGN TEAM

#### `Content title`
- [ ] ebook
- [ ] whitepaper

#### Copy
INSERT LINK TO COPY DOC

## Roles and Responsibilities
| Person | Role|
| ------ | ------ |
| `Content`  | Content/Review DRI |
| `Brand` | Design/Layout |
| @vicbell  | Illustration |

#### Due Date

*Please note the Design team has a minimum 2-week turnaround for request*

## Production
- [ ] **Content DRI:** Insert copy doc link when ready - highlight the doc with potential visuals in addition to ones already added.
- [ ] **Design DRI:** Review copy doc and create a single `Illustration` issue, adding requests as necessary.
- [ ] **Design DRI:** Create first draft PDF and add additional `Illustration` requests to that issue.
- [ ] **Content DRI:** Review first draft PDF and provide feedback.
- [ ] **Design DRI:** First draft edits, share final PDF
- [ ] **Content DRI:** Final PDF review

/label ~"design" ~"mktg-status::triage" ~"Corporate Marketing"

/assign @luke
