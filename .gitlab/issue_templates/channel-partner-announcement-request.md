### Request for review and approval of channel/technology partner-led announcement.

<!-- Requester please fill in all relevant sections. Some details may not be applicable. Please see the handbook for more information 
on requesting an announcement: https://about.gitlab.com/handbook/marketing/product-marketing/analyst-relations/channel-marketing/#when-to-reach-out-to-channel-marketing-for-gtm-support. --> 

If a channel or alliance partner is interested in taking the lead on making a formal announcement around joining the GitLab Partner Program and/or becoming a Certified Services Partner or would like to request GitLab's support/review of blog post content, please complete this issue template. 

**Please note, participation in channel partner press releases is only available for authorized Select and Distributor Partners.**

If applicable for the request, the below press release templates are a recommended guide for partners to customize based on their specific relationship with GitLab and area of expertise.
- [Joining the Partner Program (Select and Tech Alliance) Press Release Template](https://docs.google.com/document/d/12B7wwjeaU9FJqk8nT2obXS08IAJz4LAdtajeM_JMQjs/edit) 
- [Becoming a Certified Services Partner (Professional, Managed, Training) Press Release Template](https://docs.google.com/document/d/1AKQeh7u64XXLgwOjhAIi1FcUW2N5-f0McUb1bTsd1aQ/edit) 

GitLab is happy to support the partner’s announcement by providing a supporting quote from either Michelle Hodges (Channel Partners) or Nima Badiey (Alliance Partners), quote attribution based on your partner track. If the channel or alliance partner has request GitLab's support and/or review of a blog post that they are posting on their website, please share the full text blog post copy in a Google Doc for internal reviews and approvals. 

Please follow the below steps:

#### To-Do's

##### Step 1: 

For press releases, `Channel Account Manager` or `GitLab DRI for Partner` to send press release template to partner for completion (see above for template link in handbook). Once complete, send the below details and completed press release draft to `@coleengreco` and `@kpdoespr` (add `@cweaver1` as an FYI) via this issue template for GitLab internal reviews/approval.
* [ ] Partner press release template completed and link to document included below. 

For blog posts, send the below details and blog post draft in a Google Doc to `@coleengreco` and `@kpdoespr` (add `@cweaver1` as an FYI) via this issue template for GitLab internal reviews/approval.

##### Step 2: `PR Team Member` to route the partner press release through internal reviews and approval (add relevant approver names below).

* [ ] Kristi Piechnik (Christina Weaver as FYI)/CorpComms-PR 
* [ ] Coleen Greco/Channel-Partner Marketing 
* [ ] Channel Account Manager or Alliances Team Member
* [ ] Michelle Hodges or Nima Badiey

##### Step 3: `PR Team Member` to share approved press release or blog post draft including any suggested edits within 7-10 business days of request date to the partner company PR/marketing DRI and CC all GitLab DRIs on the email (channel marketing, CAM, etc). 

* [ ] Approved press release/blog post sent to partner for announcement coordination

##### Step 4: For `Requester` to complete after press release has been approved and timing has been confirmed.

###### Social Media

* [ ] If social amp is of interest, please open a new [social-general-request issue template in the Corporate Marketing Project](https://gitlab.com/gitlab-com/marketing/corporate-marketing/issues/new?issuable_template=social-general-request) and link issues.
* [ ] If a partner is leading/participating in the announcement, *please encourage them to tag our company pages on every channel they are pushing the news.* [Links to all our channels are here](https://about.gitlab.com/handbook/marketing/corporate-marketing/social-marketing/#social-channels). This makes engagement and amplification of their content easier, and helps GitLab social channels grow.

------

### Details

#### Name of the channel/alliance partner: 

[add details here]

#### Has the partner contract been finalized and signed?

- [ ] Yes
- [ ] No [If no, please note that the partner agreement/contract must be signed before making a formal announcement.]

#### If submitting for a channel partner, are they an authorized select or distributor partner?

- [ ] Yes
- [ ] No [If no, we will not be able to support a press release.]

#### Link to the completed partner press release or blog post draft.

[Included Google Doc link to the press release draft here.]

#### What is the date that the partner wants to share the announcement/content, ideally?

[add details here]

#### Is this proposed announcement date tied to a specific industry/partner event? If so, what is the date of the event?

[add details here]

#### Who is the GitLab DRI for the partner? 

[add details here]

#### Any additional GitLab SMEs who need to be a part of the review/approval process?

[add details here]

#### Who is the PR/marketing DRI at the partner company? Please share name and contact info for coordination on approvals and comms activities.

[Include name(s) and email(s)]

####  Link related issues and epics
   - [ ] Please link to all related issues and the epic

------

/label ~"corporate marketing" ~"channel marketing" ~"corp comms" ~"PR" ~"announcement" ~"mktg-status::plan" ~"channel" ~"Partner Mktg:: New Request"

/cc @cweaver1 @nwoods1 @kellypromes @coleengreco @kpdoespr

/confidential
