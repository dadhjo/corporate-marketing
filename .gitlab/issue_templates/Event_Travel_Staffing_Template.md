## :airplane: Travel
* [ ] **Travel dates:**
* [ ] **Link to hotel block or recommendation (if applicable):**
* [ ] **DEADLINE to book your travel:**
* [ ] **Travel details added to event sheet**
* Expense code: add here

## :busts_in_silhouette: Staffing and Travel
*  Make sure you have manager approval to attend
*  Book your travel at least 2 weeks in advance
*  Once you commit to an event, please make sure to plan to attend
*  Please read through our [Event Handbook pages](https://about.gitlab.com/handbook/marketing/events/#employee-booth-guidelines) for best practices at events. 

   * [ ] SAL:
   * [ ] SDR:
   * [ ] SA: If you need an SA to attend, please follow their triage process. 
   * [ ] PMM: 
   * [ ] Speaker: 
   * [ ] other:  




/label ~Events ~"Corporate Event" ~"Corporate Marketing" ~"In-Person Conference" ~"mktg-status::wip"
