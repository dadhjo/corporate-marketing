<!-- To be completed by the Corp Comms team. Some details may not be applicable.--> 
*Note: The issue template is for GitLab-led press releases. Please use the [channel/partner announcement issue template](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues/new?issuable_template=channel-partner-announcement-request) for any press release requests that will be partner-led. The press release development and review process should be kicked off at least 2 weeks prior to the planned announcement date to allow for sufficient time to get content/messaging developed and routed through the proper approval process with all key DRIs and legal.*

#### :question: Background on the Announcement

*Insert details on the purpose of the press release and announcement including product, partnership (if GitLab-led), event (etc.) details, requirements/materials needed for the launch, key stakeholders, etc.*

#### :calendar: Date/Time of Announcement and Embargo

*Insert date/time here.*

#### :notepad_spiral: Press Release DRAFT

*Include link to the G Doc of the working press release draft.*

#### :books: Helpful Resources/Content for the Announcement

*Include links to related content, analyst reports, survey data (if applicable) and messaging.*

#### :mega: Coverage

*Complete after announcement.*

------
##### Press Release Approvals:
* [ ] Corp Comms/PR team DRI
* [ ] Announcement DRI
* [ ] PMM/partner marketing DRI (if applicable)
* [ ] Product DRI (if applicable)
* [ ] Spokesperson Quoted
* [ ] Dir. Corp Comms (`@nwoods1`)
* [ ] VP of Corporate Marketing (`@melsmo`)
* [ ] CMO (`@cmestel`)
* [ ] Legal (`@rchachra` - *All of the reviews/approvals above must be complete prior to tagging in legal for final approval.*)

#### FINAL STEP **NASDAQ Disclosure**
Once the press release is legal approved, the Corp Comms DRI needs to submit the press release to NASDAQ via [this link](https://www.nasdaq.net/ED/IssuerEntry.aspx) at least 10 minutes prior to press release going live on Globenewswire. The point of contact should be the media contact for the press release. 

/label ~"corporate marketing" ~"Corp Comms" ~"PR" ~"press release" ~"mktg-status::plan"

/cc @nwoods1 @cweaver1 @kpdoespr

/confidential
